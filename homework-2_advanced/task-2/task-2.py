from dataclasses import dataclass
from datetime import datetime, timedelta
from typing import Generator, List, Tuple


@dataclass
class Movie:
    title: str
    dates: List[Tuple[datetime, datetime]]

    def schedule(self) -> Generator[datetime, None, None]:
        result = []
        for item in self.dates:
            start, end = item[0], item[-1]
            while start <= end:
                result.append(start)
                start += timedelta(days=1)
        stop_value = len(result) - 1
        current = -1
        while current < stop_value:
            current += 1
            yield result[current]


def main():

    m = Movie('sw', [
      (datetime(2020, 1, 1), datetime(2020, 1, 7)),
      (datetime(2020, 1, 15), datetime(2020, 2, 7))
    ])

    for d in m.schedule():
        print(d)


if __name__ == '__main__':
    main()
