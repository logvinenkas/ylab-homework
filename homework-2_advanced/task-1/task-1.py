class CyclicIterator:

    def __init__(self, elements):
        self.elements = elements
        self.length = len(self.elements)
        self.counter = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.counter < self.length:
            self.counter += 1
            return self.elements[self.counter - 1]
        else:
            self.counter = 1
            return self.elements[self.counter - 1]


def main():
    cyclic_iterator = CyclicIterator(range(3))
    for i in cyclic_iterator:
        print(i)


if __name__ == '__main__':
    main()
