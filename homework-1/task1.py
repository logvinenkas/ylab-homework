def domain_name(url):
  position = 0
  for garbage in ['www.', 'https://', 'http://']:
    if url.find(garbage) != -1:
      position = url.find(garbage) + len(garbage)
      break
  return url[position:].split('.')[0]


assert domain_name("http://google.com") == "google"
assert domain_name("http://google.co.jp") == "google"
assert domain_name("www.xakep.ru") == "xakep"
assert domain_name("https://youtube.com") == "youtube"