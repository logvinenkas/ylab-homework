def int32_to_ip(int32):
  bin = ''
  while int32 > 1:
    bin = str(int32 % 2) + bin
    int32 = int32 // 2
  else:
    bin = '0' * (31 - len(bin)) + str(int32) + bin

  ip = ''
  for n in range(0, 32, 8):
    part = 0
    for pos, m in enumerate(bin[n:n+8]):
      if int(m)> 0:
        part += int(m) * 2 ** (7 - pos)
    ip += str(part) + '.'
  return ip[:-1]
  
assert int32_to_ip(2154959208) == "128.114.17.104"
assert int32_to_ip(0) == "0.0.0.0"
assert int32_to_ip(2149583361) == "128.32.10.1"
assert int32_to_ip(1845231798) == "109.252.0.182"
