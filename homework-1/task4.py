from itertools import combinations


def bananas(s):
    result = set()
    keyword = 'banana'
    for sample in combinations(enumerate(s), len(keyword)):
        candidate = ['-' for _ in range(len(s))]
        n = 0
        for pos, letter in sample:
            if letter == keyword[n]:
                candidate[pos] = letter
                n += 1
            else:
                break
        if n == len(keyword):
            result.add(''.join(candidate))
    return result


assert bananas("banann") == set()
assert bananas("banana") == {"banana"}
assert bananas("bbananana") == {"b-an--ana", "-banana--", "-b--anana", "b-a--nana", "-banan--a", "b-ana--na",
                                "b---anana", "-bana--na", "-ba--nana", "b-anan--a", "-ban--ana", "b-anana--"}
assert bananas("bananaaa") == {"banan-a-", "banana--", "banan--a"}
assert bananas("bananana") == {"ban--ana", "ba--nana", "bana--na", "b--anana", "banana--", "banan--a"}
