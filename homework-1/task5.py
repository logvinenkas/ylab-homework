from math import prod

def count_find_num(primesL, limit):
    n = prod(primesL)
    if n > limit: 
      return []
    L = [n]
    for p in primesL:
        for n in L:
            a = p * n
            while a <= limit:
                L.append(a)
                a *= p
    return [len(set(L)),max(L)]

primesL = [2, 3]
limit = 200
assert count_find_num(primesL, limit) == [13, 192]

primesL = [2, 5]
limit = 200
assert count_find_num(primesL, limit) == [8, 200]

primesL = [2, 3, 5]
limit = 500
assert count_find_num(primesL, limit) == [12, 480]

primesL = [2, 3, 5]
limit = 1000
assert count_find_num(primesL, limit) == [19, 960]

primesL = [2, 3, 47]
limit = 200
assert count_find_num(primesL, limit) == []
