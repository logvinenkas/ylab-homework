from itertools import permutations


def main(points):
    combinations = permutations(points[1:])
    record_duration = record_way_segments = None
    for combination in combinations:
        duration = 0
        way = list(combination)
        way.insert(0, points[0])
        way.append(points[0])
        way_segments = str(way[0]) + ' -> '
        for i in range(len(way) - 1):
            step_duration = ((way[i + 1][0] - way[i][0]) ** 2 + (way[i + 1][1] - way[i][1]) ** 2) ** 0.5
            duration += step_duration
            way_segments += f"{way[i + 1]}[{step_duration}] -> "
            if record_duration and duration > record_duration:
                break
        if record_duration is None or record_duration > duration:
            record_duration = duration
            record_way_segments = way_segments[:-4] + ' = ' + str(record_duration)

    return record_way_segments


if __name__ == '__main__':
    all_points = [(0, 2), (2, 5), (5, 2), (6, 6), (8, 3)]
    print(main(all_points))
