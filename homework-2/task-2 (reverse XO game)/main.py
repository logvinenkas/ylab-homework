import os
import random
from sys import platform

SIZE = 10
PLAY_WITH_MONKEY = True

FAIL_COUNT = int(SIZE / 2)
ALPHABET = [(chr(letter)) for letter in range(ord('а'), ord('а') + 32)]


def pc_turn(game, player_sign):
    def random_suicide_monkey_turn(cells_list):
        choice = random.choice(cells_list)
        return choice

    def random_monkey_turn(cells_list):
        turn = None
        for i in range(len(cells_list)):
            turn = random.choice(cells_list)
            cells_list.remove(turn)
            i -= 1
            game[turn] = player_sign
            if check_fail(game) is not None:
                game[turn] = None
            else:
                game[turn] = None
                return turn
        return turn

    free_cells = list()
    for pos, item in enumerate(game):
        if item is None:
            free_cells.append(pos)

    next_turn = random_monkey_turn(free_cells)
    if next_turn is None:
        return random_suicide_monkey_turn(free_cells)
    else:
        return next_turn


def clear_screen():
    try:
        if platform.startswith('linux'):
            os.system('clear')
        elif platform.startswith('win32'):
            os.system('cls')
        elif platform.startswith('darwin'):
            os.system("printf '\\33c\\e[3J'")
        else:
            raise Exception
    except Exception as e:
        print(f'Упс! Обновление окна консоли не работает на данной операционной системе, '
              f'так как я не успел протестировать каждую из них\n'
              f'Передайте мне, пожалуйста, эту информацию и я все починю:\n\n'
              f'Ошибка на ОС "{platform}": {e}')


def show_field(field):
    n = 0
    while n < SIZE:
        print('\t' + ALPHABET[n], end='')
        n += 1
    print('\n')  # запуск из консоли
    # print('')  # запуск из pycharm

    for n, i in enumerate(field):
        position = n + 1
        print(str(position // SIZE) + '\t', end='') if n % SIZE == 0 else None
        print('_', end='\t') if i is None else print(i, end='\t')
        print('\n') if position % len(field) ** 0.5 == 0 else None


def check_fail(field):
    for pos, marker in enumerate(field):
        if marker:
            if pos % SIZE <= SIZE - FAIL_COUNT:

                # проверка по горизонтали --
                current_fail = 0
                for next_check in field[pos:pos + FAIL_COUNT]:
                    if next_check == marker:
                        current_fail += 1
                    else:
                        break
                    if current_fail == FAIL_COUNT:
                        return True, marker

                # проверка по диагонали \
                current_fail = 0
                for posit2, next_check in enumerate(field[pos::SIZE + 1]):
                    if next_check == marker:
                        current_fail += 1
                    else:
                        break
                    if current_fail == FAIL_COUNT:
                        return True, marker

            # проверка по диагонали /
            if pos % SIZE + 1 >= SIZE - FAIL_COUNT:
                current_fail = 0
                for posit2, next_check in enumerate(field[pos::SIZE - 1]):
                    if next_check == marker:
                        current_fail += 1
                    else:
                        break
                    if current_fail == FAIL_COUNT:
                        return True, marker

            # проверка по вертикали |
            if pos <= len(field) / 2 + SIZE:
                current_fail = 0
                for posit2, next_check in enumerate(field[pos::SIZE]):
                    if next_check == marker:
                        current_fail += 1
                    else:
                        break
                    if current_fail == FAIL_COUNT:
                        return True, marker


def main():
    os.environ.setdefault('TERM', 'xterm')
    current_game = list([None for _ in range(SIZE ** 2)])

    fail = None
    while True:
        intro = 'Выберите режим отображения маркеров:\n\n' \
                '\t(1) Символьный (стандартный)\n' \
                '\t(2) Эмодзи (комфортнее, но может иметь недостатки отображения на некоторых ОС\n'
        print(intro)
        marker_mode = input('Ваш выбор: ')
        if marker_mode.isdigit() and int(marker_mode) == 1:
            players = ['O', 'X']
            break
        elif marker_mode.isdigit() and int(marker_mode) == 2:
            players = ['⭕', '❎']
            break
        else:
            clear_screen()
            print('Ошибка ввода: требуется цифра.', end=' ')

    step = 0
    clear_screen()
    show_field(current_game)
    pc_turn_history = list()

    # игровой цикл, пока кто-то не проиграл
    while fail is None:
        string = row = None

        # цикл проверки ввода данных от пользователя
        while True:
            step_player = input(f'Ход игрока "{players[step]}" (цифра-буква/буква-цифра): ')
            if len(step_player) == 2:
                if step_player[0].isdigit() and step_player[1].isalpha():
                    string = step_player[0]
                    row = step_player[1]
                elif step_player[1].isdigit() and step_player[0].isalpha():
                    string = step_player[1]
                    row = step_player[0]
                else:
                    print('Ошибка. Нужна буква и цифра или наоборот')
            else:
                print('Ошибка. Нужны строго 2 символа')

            if string and row:
                if 0 <= int(string) < SIZE and row in ALPHABET[0:SIZE]:
                    alpha_string = ''.join(x for x in ALPHABET[0:SIZE])
                    row = alpha_string.find(row.lower())
                    if current_game[int(string) * 10 + row] is None:
                        current_game[int(string) * 10 + row] = players[step]
                        clear_screen()
                        break
                    else:
                        print(f'Ошибка. Ячейка занята. Выберите другую')
                else:
                    print(f'Ошибка. Допустимые буквы от "{ALPHABET[0]}" до "{ALPHABET[SIZE - 1]}" и '
                          f'цифры от 0 до {SIZE - 1}')

        # проверяем проигрышные позиции на текущем поле
        fail = check_fail(current_game)
        show_field(current_game)
        if fail is not None:
            print(f'\nPlayer "{fail[1]}" failed the game')
            break

        # меняем игрока
        step = 1 if step == 0 else 0

        if PLAY_WITH_MONKEY is True:
            pc_turn_index = pc_turn(current_game, players[step])
            current_game[pc_turn_index] = players[step]
            # меняем игрока
            step = 1 if step == 0 else 0
            clear_screen()
            #   Записываем историю ходов и выводим ее в консоль
            pc_turn_history.append(f"{ALPHABET[pc_turn_index % SIZE]}{pc_turn_index // SIZE}")
            print(f'Ходы соперника: ', end='')
            for index, turn in enumerate(pc_turn_history):
                print(turn + ' ', end='')
                if index > 0 and index % (SIZE * 2 + 1) == 0:
                    print('\n', end='\t\t')
            print('\n')

            # проверяем проигрышные позиции на текущем поле
            fail = check_fail(current_game)
            show_field(current_game)
            if fail is not None:
                print(f'\nPlayer "{fail[1]}" failed the game')
                break
            elif None not in current_game:
                print(f'\nDraw!')
                break


if __name__ == '__main__':
    main()
