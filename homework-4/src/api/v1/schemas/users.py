from pydantic import BaseModel
from typing import Optional

__all__ = (
    "UserBase",
    "UserSignUp",
    "UserSignIn",
    "UserInfo"
)


class UserBase(BaseModel):
    username: str


class UserSignIn(UserBase):
    password: str


class UserSignUp(UserSignIn):
    email: str


class UserInfo(BaseModel):
    username: Optional[str] = None
    email: Optional[str] = None
