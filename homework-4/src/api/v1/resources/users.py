from fastapi import APIRouter, Depends

from src.api.v1.schemas.users import UserInfo
from src.services.user import UserService, get_user_service, get_current_user
from src.models import User

router = APIRouter()


@router.get(
    path="/me",
    response_model=User,
    summary="Просмотр информации о себе",
    tags=["users"],
)
def get_user(
        user: User = Depends(get_current_user),
):
    return user


@router.patch(
    path="/me",
    response_model=User,
    summary="Обновление информации о себе",
    tags=["users"],
)
def update_user(
        user_update: UserInfo,
        user: User = Depends(get_current_user),
        user_service: UserService = Depends(get_user_service),
):
    return user_service.update_user(user, user_update)
