from abc import ABC, abstractmethod
from dataclasses import dataclass
from weapons import Guns, Lasers, Kicks


@dataclass
class Hero(ABC):
    name: str

    @abstractmethod
    def attack(self):
        pass


class SuperHero(Hero):

    @abstractmethod
    def ultimate(self):
        pass


class ChackNorris(Guns, Kicks, Hero):

    def __init__(self):
        self.name = 'Chack Norris'

    def attack(self):
        self.roundhouse_kick()
        self.fire_a_gun()


class Superman(Guns, Lasers, Kicks, SuperHero):

    def __init__(self):
        self.name = 'Clark Kent'

    def attack(self):
        self.roundhouse_kick()
        self.fire_a_gun()
        self.ultimate()

    def ultimate(self):
        self.incinerate_with_lasers()

#     # Проблема: Герой не должен заниматься оповещениями о своей победе, это задача масс-медиа.
#     # Не соблюден: Принцип единой ответственности.
#     # По SOLID: Вынести оповещение в отдельный класс, занимающийся выводом информации.
#     # Когда возникнут трудности? Добавьте оповещение о победе героя через газеты или через TV (на выбор)
#     # а также попробуйте оповестить планеты (у которых вместо атрибута name:str используется coordinates:List[float]).
#     def create_news(self, place):
#         place_name = getattr(place, 'name', 'place')
#         print(f'{self.name} saved the {place_name}!')
