from abc import ABC, abstractmethod
from dataclasses import dataclass


@dataclass
class PLace(ABC):
    name: str or list

    @abstractmethod
    def get_antagonist(self):
        pass


class Kostroma(PLace):

    def __init__(self):
        self.name = 'Kostroma'

    def get_antagonist(self):
        print('Orcs hid in the forest')


class Tokyo(PLace):

    def __init__(self):
        self.name = 'Tokyo'

    def get_antagonist(self):
        print('Godzilla stands near a skyscraper')


class Earth(PLace):

    def __init__(self):
        self.name = [1212, 123123]

    def get_antagonist(self):
        print('Godzilla stands near a skyscraper')
