from save_the_place import SaveThePlace
from heroes import Superman, ChackNorris
from places import Kostroma, Tokyo, Earth
from massmedia import Newspaper, TV


if __name__ == '__main__':
    SaveThePlace(Superman(), Kostroma(), Newspaper())
    print('-' * 20)
    SaveThePlace(ChackNorris(), Tokyo(), TV())
    print('-' * 20)
    SaveThePlace(ChackNorris(), Earth(), TV())

# Orcs hid in the forest
# Wzzzuuuup!
# Clark Kent saved the place!
# --------------------
# Godzilla stands near a skyscraper
# PIU PIU
# Chack Norris saved the Tokyo!
