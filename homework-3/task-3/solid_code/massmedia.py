from abc import ABC, abstractmethod


class MassMedia(ABC):

    @abstractmethod
    def create_news(self, text):
        pass


class Newspaper(MassMedia):
    def create_news(self, text):
        print(f'Hot newspaper news: {text}')


class TV(MassMedia):
    def create_news(self, text):
        print(f'Breaking TV news: {text}')
