from heroes import Hero
from places import PLace
from massmedia import MassMedia


class SaveThePlace:

    def get_enemy(self):
        return self.place.get_antagonist()

    def push_news(self):
        self.massmedia.create_news(f'{self.hero.name} saved the {self.place.name}!')

    def __init__(self, hero: Hero, place: PLace, massmedia: MassMedia):
        self.place = place
        self.hero = hero
        self.massmedia = massmedia

        self.get_enemy()
        hero.attack()
        self.push_news()
