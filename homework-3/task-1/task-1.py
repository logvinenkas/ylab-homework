def hash_value(function_to_decorate):
    hash_list: dict = {}

    def wrapper(argument):
        if argument in hash_list.keys():
            return hash_list.get(argument)

        hash_list[argument] = function_to_decorate(argument)
        return function_to_decorate(argument)

    return wrapper


@hash_value
def multiplier(number: int):
    return number * 2


def main():
    print(multiplier(4))
    print(multiplier(4))


if __name__ == '__main__':
    main()
