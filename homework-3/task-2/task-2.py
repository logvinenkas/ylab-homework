import time


def duplicate_respect(function_to_decorate):
    def wrapper(call_count=1, start_sleep_time=0, factor=0, border_sleep_time=0):
        print('Кол-во запусков =', call_count, '\nНачало работы')
        t = start_sleep_time
        for duplicate in range(call_count):
            decorated_func_result = function_to_decorate()
            print(f'Запуск номер {duplicate + 1}. Ожидание: {t} секунд. '
                  f'Результат декорируемой функций = {decorated_func_result}.')
            # t = start_sleep_time * 2 ** factor
            t = start_sleep_time * factor
            t = border_sleep_time if t >= border_sleep_time else t
            start_sleep_time = t
            time.sleep(t)
        print('Конец работы')
    return wrapper


@duplicate_respect
def some_cool_function():
    respect = True
    return respect


def main():
    some_cool_function(call_count=20, start_sleep_time=1, factor=2, border_sleep_time=500000)


if __name__ == '__main__':
    main()
